<?php
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration Form</title>
</head>
<body>
        <div class="form">
            <form action="store.php" method="post">
                <h3>Applicant's Information</h3>
                <level>Name of Applicant:<spam>*</spam></level>
                <input type="text" name="name" placeholder="Name">

                <level>Email:<spam>*</spam></level>
                <input type="email" name="email" placeholder="Email">

                <level>Birth Date:<spam>*</spam></level>
                <input type="date" name="birth_data" >

                <level>Religion:<spam>*</spam></level>
                <input type="email" name="email" placeholder="Email">

                <level>Phone:<spam>*</spam></level>
                <input type="text" name="phone" placeholder="Phone">

                <h3>Parent's Information</h3>
                <level>Father Name:<spam>*</spam></level>
                <input type="text" name="father_name" placeholder="Father Name">

                <level>Mother Name:<spam>*</spam></level>
                <input type="text" name="mother_name" placeholder="Mother Name">

                <level>Guardian's /Parent's Phone:<spam>*</spam></level>
                <input type="text" name="gurdian_phone" placeholder="Phone">
            </form>

        </div>
</body>
</html>
