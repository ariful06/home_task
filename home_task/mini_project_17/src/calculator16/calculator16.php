<?php

namespace test\calculator16;
use PDO;
class calculator16
{
    public $num1= '';
    public $num2= '';
    public $operator = '';
    public  $results = '';
        public function setData($a='',$b='',$sign=''){

            $this->num1 = $a;
                $this->num2 = $b;
                $this->operator= $sign;
        }
        public function add(){
             return $this->results = $this->num1+$this->num2;
        }
    public function sub(){
        return $this->results = $this->num1-$this->num2;
    }
    public function mul(){
        return $this->results = $this->num1*$this->num2;
    }
    public function div(){
        return $this->results = $this->num1/$this->num2;
    }
        public function calculate(){
            if(!empty($this->num1) && !empty($this->num2)){
                if($this->operator == 'add'){
                    echo   $this->add();
                }
                if($this->operator == 'sub'){
                    echo   $this->sub();
                }
                if($this->operator == 'mul'){
                    echo   $this->mul();
                }
                if($this->operator == 'div'){
                    echo   $this->div();
                }
            }
        }
        public function store(){
           try{

               $pdo = new PDO ('mysql:host=localhost;dbname=calculator','root','');
               $query = "INSERT INTO results (`number1`,`number2`,`results`) VALUES (:number1,:number2,:results)";
               $statement = $pdo->prepare($query);
               $statement->execute(array(
                   ':number1' => $this->num1,
                   ':number2' => $this->num2,
                   ':results' => $this->results
                   ));
               session_start();
               $_SESSION['message'] ="sucsessfully added into the database";
               header("location:create.php");
           }
           catch (PDOException $e){
               echo "Error" . $e->getMessage();
           }

        }
}