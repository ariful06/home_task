<?php
include_once ("../../vendor/autoload.php");
use test\calculator16\calculator16;

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    if(!empty($_POST['num1']) && !empty($_POST['num2'] && isset($_POST['num1'] , $_POST['num2'])) ) {
        $object_of_calculator16 = new calculator16();
        $object_of_calculator16->setData($_POST['num1'], $_POST['num2'], $_POST['select']);
        $object_of_calculator16->calculate();
        $object_of_calculator16->store();
    }
    else{
        session_start();
        $_SESSION['message'] ="Invalid input";
        header("location:create.php");
    }
}

else{
    header('location:create.php');
}