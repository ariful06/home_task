<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitae72b8660803fa1a86d70c8351306d4d
{
    public static $prefixLengthsPsr4 = array (
        't' => 
        array (
            'test\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'test\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitae72b8660803fa1a86d70c8351306d4d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitae72b8660803fa1a86d70c8351306d4d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
