<?php
namespace App\Service;
use PDO;
class Service
{
    private $title='';
    private $description='';
    private $user_id ='';
    private $img ='';
    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }
    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('dir', $data)) {
            echo  $this->img = $data['dir'];
        }
        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        return $this;
    }
    public function store()
    {
        session_start();
        try {
            $query2="INSERT INTO `services`(`id`,`user_id`,`title`, `description`,`img`) VALUES (:id,:user_id,:title,:description,:img)";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':id'=>null,
                ':user_id'=>$_SESSION['user-info']['id'],
                ':title' => $this->title,
                ':description' => $this->description,
                'featured_img'=>$this->img

            ));
            if ($stmt) {
                session_start();
                $_SESSION['msg'] = "Successfully Inserted !";
                header('location:view.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index()
    {
        session_start();
        try {
            $query_for_show = "SELECT * FROM `services` WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute();
            $test = $stmt->fetch();
            $_SESSION['settings'] = $test;
        }
        catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show()
    {
        try{
            $query = "SELECT * FROM `services` WHERE `user_id` = "."'".$this->user_id."'";

            $show = $this->pdo->prepare($query);
            $show->execute();
            $value = $show->fetch();

            return $value;

        }catch (PDOException $e)
        {
            echo 'Error:' . $e->getMessage();
        }
    }
    public function update(){

        try {
            $query_for_show = "UPDATE `services` SET `title`=:title,`description`=:description,`img`=:img WHERE `user_id`="."'".$this->user_id."'";

            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute(array(
                ':title'=>$this->title,
                ':description'=>$this->description,
                ':img'=>$this->img
            ));
            if($stmt) {
                session_start();
                $_SESSION['message'] = "sucsessfully updated";
                header("location:view.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function deleted(){
        session_start();
        try {
            $query ="DELETE FROM `services` WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "sucsessfully deleted";
                header("location:add.php");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}