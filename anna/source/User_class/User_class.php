<?php
namespace App\User_class;
use PDO;
class User_class
{
    private $username='';
    private $email='';
    private $password='';
    private $token='';
    private $user_id='';
    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }
    public function setData($data = '')
    {

        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('token', $data)) {
            $this->token = $data['token'];
        }
        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }

        return $this;

    }
    public function validity()
    {
        try {
            $query = "SELECT  * FROM `users` where username='$this->usernam' OR email='$this->email'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }
    public function emailVerification()
    {
        try {
            $query = "SELECT  * FROM `users` where token='$this->token'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if (!empty($data)) {
                $query = 'UPDATE `users` SET is_active = 1 WHERE token = :token';
                $stmt = $this->pdo->prepare($query);
                $stmt->execute(array(
                    ':token' => $this->token
                ));
                $_SESSION['message'] = "Email Verified. You can login now";
                header('location:loginForm.php');

            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
    public function login()
    {
        try {
         $query = "SELECT  * FROM `users` WHERE username='$this->username' AND password=".$this->password;

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
//            echo "<pre>";
//            print_r($data);

            if (empty($data)) {
                session_start();
                $_SESSION['fail'] = "Opps! User email does not match";
                header('location:loginForm.php');
            } else if ($data['is_active'] == 0) {
                session_start();
                $_SESSION['fail'] = "You are not authorized please sign up than log in";
                header('location:loginForm.php');
            } else {
                session_start();
                $_SESSION['user_info'] = $data;
//                print_r( $_SESSION['user_info']);
                header('location:dashboard.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function store(){
        $token=sha1($this->username);
        try{
            $query="INSERT INTO `users` (`id`, `unique_id`, `username`, `email`, `password`, `token`, `is_active`,`user_role`) VALUES(:id,:unique_id,:user_name, :email_address,:password, :token, :is_active,:ur)";
            $stmt=$this->pdo->prepare($query);
            $output=$stmt->execute(
                array(
                    ":id"=>null,
                    ":unique_id"=>uniqid(),
                    ":user_name"=>$this->username,
                    ":email_address"=>$this->email,
                    ":password"=>$this->password,
                    ":token"=>$token,
                    ":is_active"=>0,
                    ':ur'=> 1
                )
            );
            $last_id = $this->pdo->lastInsertId();

            $sql_for_at = "INSERT INTO `abouts` (`user_id`) VALUES (:user_id)";
            $stmt=$this->pdo->prepare($sql_for_at);
            $stmt->execute(
                array( ":user_id"=>$last_id ));
            $sql_for_at = "INSERT INTO `settings` (`user_id`) VALUES (:user_id)";
            $stmt=$this->pdo->prepare($sql_for_at);
            $stmt->execute(
                array( ":user_id"=>$last_id ));

            $txt="http://localhost/anna/views/varify.php?token=$token";
//            echo $txt;
//            die();
            $subject= "Email Varification..";
            mail($this->email,$subject,$txt);
            if($output){
                $_SESSION['message']="Registration Successfully !";
                header('location:loginForm.php');

            }
        }catch (PDOException $e){
            echo "ERROR :".$e->getMessage();
        }
    }


}