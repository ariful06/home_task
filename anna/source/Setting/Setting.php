<?php
namespace App\Setting;
use PDO;
class Setting
{
    private $title='';
    private $fullname='';
    private $description='';
    private $address='';
    private $user_id ='';
    private $featured_img ='';
    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }
    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('fullname', $data)) {
            $this->fullname = $data['fullname'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('address', $data)) {
            echo $this->address = $data['address'];
        }
        if (array_key_exists('dir', $data)) {
            echo  $this->featured_img = $data['dir'];
        }
        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        return $this;
    }
    public function store()
    {
        session_start();
        try {
            $query2="INSERT INTO `settings`(`id`, `user_id`, `title`, `fullname`, `description`, `address`,`featured_img`) VALUES (:id,:user_id,:title,:fullname,:description,:address,:featured_img)";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':id'=>null,
                ':user_id' => $_SESSION['user_info']['id'],
                'featured_img'=>$this->featured_img,
                ':title' => $this->title,
                ':fullname' => $this->fullname,
                ':description' => $this->description,
                ':address' => $this->address
            ));
            if ($stmt) {
                session_start();
                $_SESSION['msg'] = "Successfully Inserted !";
                header('location:view.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index()
    {
        session_start();
        try {
            $query_for_show = "SELECT * FROM `settings` WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute();
            $test = $stmt->fetch();
          return $test;
        }
        catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show()
    {
        echo $this->user_id;
        try{
            $query = "SELECT * FROM `settings` WHERE `user_id` = "."'".$this->user_id."'";

            $show = $this->pdo->prepare($query);
            $show->execute();
            $value = $show->fetch();

            return $value;

        }catch (PDOException $e)
        {
            echo 'Error:' . $e->getMessage();
        }
    }
    public function update(){

        try {
            $query_for_show = "UPDATE `settings` SET `title`=:title,`featured_img`=:featured_img,`fullname`=:fullname,`description`=:description,`address`=:address WHERE `user_id`="."'".$this->user_id."'";

            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute(array(
                ':title'=>$this->title,
                ':featured_img'=>$this->featured_img,
                ':fullname'=>$this->fullname,
                ':description'=>$this->description,
                ':address'=>$this->address,
            ));
            if($stmt) {
                session_start();
                $_SESSION['message'] = "sucsessfully updated";
                header("location:view.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function deleted(){
        session_start();
        try {
            $query ="DELETE FROM `settings` WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "sucsessfully deleted";
                header("location:add.php");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}