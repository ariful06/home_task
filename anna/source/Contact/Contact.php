<?php
namespace App\Contact;
use PDO;
class Contact
{
    private $name='';
    private $email='';
    private $message='';
    private $phone ='';
    private $user_id = '';
    public function __construct()
    {

        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }
    public function setData($data = '')
    {
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('message', $data)) {
            $this->message = $data['message'];
        }

        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }



        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        return $this;

    }
    public function store()
    {
        session_start();
        try {
            $query2="INSERT INTO `contacts`(`user_id`, `name`, `email`, `message`, `phone`) VALUES(:user_id,:name,:email,:message,:phone)";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':user_id' => $_SESSION['user_info']['id'],
                ':name' => $this->name,
                ':email' => $this->email,
                ':message' => $this->message,
                ':phone' => $this->phone,

            ));
            if ($stmt) {
                session_start();
                $_SESSION['msg'] = "Successfully Inserted !";
                header('location:view.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index()
    {
        try {

            $query_for_show = "SELECT * FROM `contacts` WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute();
            $test = $stmt->fetch();

            return $test;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
        try {
            $query2="UPDATE `contacts` SET `user_id`=:user_id,`name`=:name,`email`=:email,`message`=:message,`phone`=:phone WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':user_id' => $_SESSION['user_info']['id'],
                ':name' => $this->name,
                ':email' => $this->email,
                ':message' => $this->message,
                ':phone' => $this->phone,
            ));
            if($stmt) {
                session_start();
                $_SESSION['message'] = "sucsessfully updated";
                header("location:view.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function deleted(){
        session_start();
        try {
            $query ="DELETE FROM `contacts` WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "sucsessfully deleted";
                header("location:add.php");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

}