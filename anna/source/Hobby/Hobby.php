<?php
namespace App\Hobby;
use PDO;
class Hobby
{
    private $title='';
    private $description='';
    private $img='';
    private $user_id = '';

    public function __construct()
    {

        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('img', $data)) {
            $this->img = $data['img'];
        }

        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        return $this;

    }
    public function store()
    {
        session_start();
        try {
            $query2="INSERT INTO `hobbies`(`user_id`, `title`, `description`, `img`) VALUES (:user_id,:title,:description,:img)";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':user_id' => $_SESSION['user_info']['id'],
                ':title' => $this->title,
                ':description' => $this->description,
                ':img' => $this->img,


            ));
            if ($stmt) {
                session_start();
                $_SESSION['msg'] = "Successfully Inserted !";
                header('location:view.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index()
    {
        try {

            $query = "SELECT * FROM `hobbies` WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $test = $stmt->fetch();

            return $test;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
        try {
            $query2="UPDATE `hobbies` SET `user_id`=:user_id,`title`=:title,`description`=:description,`img`=:img  WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':user_id' => $_SESSION['user_info']['id'],
                ':title' => $this->title,
                ':description' => $this->description,
                ':img' => $this->img,

            ));
            if($stmt) {
                session_start();
                $_SESSION['message'] = "sucsessfully updated";
                header("location:view.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function deleted(){
        session_start();
        try {
            $query ="DELETE FROM `hobbies` WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "sucsessfully deleted";
                header("location:add.php");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }


}