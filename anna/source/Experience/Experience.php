<?php
namespace App\Experience;
use PDO;
class Experience
{


    private $designation='';
    private $company_name='';
    private $start_date='';
    private $end_date ='';
    private $company_location ='';
    private $user_id = '';
    public function __construct()
    {

        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }
    public function setData($data = '')
    {
        if (array_key_exists('designation', $data)) {
            $this->designation = $data['designation'];
        }
        if (array_key_exists('company_name', $data)) {
            $this->company_name = $data['company_name'];
        }
        if (array_key_exists('start_date', $data)) {
            $this->start_date = $data['start_date'];
        }

        if (array_key_exists('end_date', $data)) {
            $this->end_date = $data['end_date'];
        }
        if (array_key_exists('company_location', $data)) {
            $this->company_location = $data['company_location'];
        }

        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        return $this;

    }
    public function store()
    {
        session_start();
        try {
            $query2="INSERT INTO `experiences`(`user_id`, `designation`, `company_name`, `start_date`, `end_date`, `company_location`) VALUES(:user_id,:designation,:companyN,:start_date,:end_date,:companyL)";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':user_id' => $_SESSION['user_info']['id'],
                ':designation' => $this->designation,
                ':companyN' => $this->company_name,
                ':start_date' => $this->start_date,
                ':end_date' => $this->end_date,
                ':companyL' => $this->company_location,

            ));
            if ($stmt) {
                session_start();
                $_SESSION['msg'] = "Successfully Inserted !";
                header('location:view.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index()
    {
        try {

            $query_for_show = "SELECT * FROM `experiences` WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute();
            $test = $stmt->fetch();

            return $test;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
        try {
            $query2="UPDATE `experiences` SET `user_id`=:user_id,`designation`=:designation,`company_name`=:companyN,`start_date`=:start_date,`end_date`=:end_date,`company_location`=:companyL  WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':user_id' => $_SESSION['user_info']['id'],
                ':designation' => $this->designation,
                ':companyN' => $this->company_name,
                ':start_date' => $this->start_date,
                ':end_date' => $this->end_date,
                ':companyL' => $this->company_location,

            ));
            if($stmt) {
                session_start();
                $_SESSION['message'] = "sucsessfully updated";
                header("location:view.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function deleted(){
        session_start();
        try {
            $query ="DELETE FROM `experiences` WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "sucsessfully deleted";
                header("location:add.php");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

}