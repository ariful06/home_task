<?php
namespace App\About;
use PDO;
class About
{

    private $title='';
    private $phone='';
    private $bio='';
    private $user_id ='';
    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }
    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('bio', $data)) {
            $this->bio = $data['bio'];
        }

        if (array_key_exists('user_id', $data)) {
           $this->user_id = $data['user_id'];

        }
        return $this;

    }
//    public function store()
//    {
//        session_start();
//        try {
//            $query2="INSERT INTO `abouts`( `user_id`, `title`, `phone`, `bio`) VALUES (:user_id,:title,:phone,:bio)";
//            $stmt = $this->pdo->prepare($query2);
//            $stmt->execute(array(
//                ':user_id' => $_SESSION['user_info']['id'],
//                ':title' => $this->title,
//                ':phone' => $this->phone,
//                ':bio' => $this->bio,
//            ));
//            if ($stmt) {
//                session_start();
//                $_SESSION['msg'] = "Successfully Inserted !";
//                header('location:view.php');
//            }
//        } catch (PDOException $e) {
//            echo 'Error: ' . $e->getMessage();
//        }
//    }
    public function index()
    { session_start();
        try {
            $query_for_show = "SELECT * FROM `abouts` WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
        try {
            $query_for_show = "UPDATE `abouts` SET `title`=:title,`phone`=:phone,`bio`=:bio WHERE `user_id`=".$this->user_id;
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute(array(
                ':title'=>$this->title,
                ':phone'=>$this->phone,
                ':bio'=>$this->bio
            ));
            if($stmt) {
                session_start();
                $_SESSION['message'] = "sucsessfully updated";
                header("location:view.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function deleted(){
        session_start();
        try {
            $query ="DELETE FROM `abouts` WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "sucsessfully deleted";
                header("location:add.php");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
}