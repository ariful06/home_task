<?php
namespace App\Education;
use PDO;
class Education
{
    private $title='';
    private $institute='';
    private $result='';
    private $passing_year ='';
    private $main_subject ='';
    private $education_board ='';
    private $course_duration ='';
    private $user_id ='';

    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }
    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('institute', $data)) {
            $this->institute = $data['institute'];
        }
        if (array_key_exists('result', $data)) {
            $this->result = $data['result'];
        }

        if (array_key_exists('Passing_year', $data)) {
            $this->passing_year = $data['Passing_year'];
        }
        if (array_key_exists('main_subject', $data)) {
            $this->main_subject = $data['main_subject'];
        }
        if (array_key_exists('Education_board', $data)) {
            $this->education_board = $data['Education_board'];
        }
        if (array_key_exists('course_duration', $data)) {
            $this->course_duration = $data['course_duration'];
        }
        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        return $this;

    }
    public function store()
    {

        session_start();
        try {
            $query2="INSERT INTO `educations`(`user_id`, `title`, `institute`, `result`, `passing_year`, `main_subject`, `education_board`, `course_duration`)VALUES(:user_id,:title,:instutie,:result,:pass_yr,:main_sub,:edu_b,:course_d)";
            $stmt = $this->pdo->prepare($query2);

            $stmt->execute(array(

                ':user_id' => $_SESSION['user_info']['id'],
                ':title' => $this->title,
                ':instutie' => $this->institute,
                ':result' => $this->result,
                ':pass_yr' => $this->passing_year,
                ':main_sub' => $this->main_subject,
                ':edu_b' => $this->education_board,
                ':course_d' => $this->course_duration,

            ));

            if ($stmt) {
                session_start();
                $_SESSION['msg'] = "Successfully Inserted !";
                header('location:view.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index()
    {
        session_start();
        try {
            $query_for_show = "SELECT * FROM `educations` WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute();
            $test = $stmt->fetch();

            $_SESSION['education'] = $test;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){

        try {
            $query_for_show = "UPDATE `educations` SET `user_id`=:user_id,`title`=:title,`institute`=:institute,`result`=:result,`passing_year`=:passing_year,`main_subject`=:main_subject,`education_board`=:education_board,`course_duration`=:course_duration WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute(array(
                ':user_id' => $_SESSION['user_info']['id'],
                ':title' => $this->title,
                ':institute' => $this->institute,
                ':result' => $this->result,
                ':passing_year' => $this->passing_year,
                ':main_subject' => $this->main_subject,
                ':education_board' => $this->education_board,
                ':course_duration' => $this->course_duration,

            ));
            if($stmt) {
                session_start();
                $_SESSION['message'] = "sucsessfully updated";
                header("location:view.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function deleted(){
        session_start();
        try {
            $query ="DELETE FROM `educations` WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "sucsessfully deleted";
                header("location:add.php");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

}