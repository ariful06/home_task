<?php
namespace App\Skill;
use PDO;
class Skill
{
    private $title='';
    private $description='';
    private $level='';
    private $experience ='';
    private $experience_area= '';
    private $session= '';
    private $user_id = '';
    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }
    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('level', $data)) {
            $this->level = $data['level'];
        }
        if (array_key_exists('experience', $data)) {
            $this->experience = $data['experience'];
        }
        if (array_key_exists('experience_area', $data)) {
            $this->experience_area = $data['experience_area'];
        }

        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        return $this;

    }
    public function store()
    {
        session_start();
        try {
            $query2="INSERT INTO `skills`( `title`,`user_id`,`description`, `level`, `experience`,`experience_area`) VALUES (:title,:user_id,:description,:level,:experience,:experience_area)";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':title' => $this->title,
                ':description' => $this->description,
                ':level' => $this->level,
                ':user_id' => $_SESSION['user_info']['id'],
                ':experience' => $this->experience,
                ':experience_area' => $this->experience_area,
            ));
            if ($stmt) {
                session_start();
                $_SESSION['message'] = "Successfully Inserted !";
                header('location:view.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index()
    {session_start();
        try {
            $query_for_show = "SELECT * FROM `skills` WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
session_start();
        try {
            $query_for_show = "UPDATE `skills` SET `title`=:title,`description`=:description,`level`=:level,`experience=:experience`,`experience_area=:experience_area` WHERE `user_id`="."'".$_SESSION['user_info']['id']."'";
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute(array(
                ':title' => $this->title,
                ':description' => $this->description,
                ':level' => $this->level,
                ':experience' => $this->experience,
                ':experience_area' => $this->experience_area,
            ));
            if($stmt) {
                session_start();
                $_SESSION['message'] = "sucsessfully updated";
                header("location:view.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function deleted(){
        session_start();
        try {
            $query ="DELETE FROM `skills` WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "sucsessfully deleted";
                header("location:add.php");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
}