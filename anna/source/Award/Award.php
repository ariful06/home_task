<?php
namespace App\Award;
use PDO;
class Award

{
    private $title='';
    private $organization='';
    private $description='';
    private $location ='';
    private $year = '';
    private $user_id = '';
    public function __construct()
    {

        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }
    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('organization', $data)) {
            $this->organization = $data['organization'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }

         if (array_key_exists('location', $data)) {
            $this->location = $data['location'];
        }

        if (array_key_exists('year', $data)) {
            $this->year = $data['year'];
        }

        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        return $this;

    }
    public function store()
    {
        session_start();
        try {
            $query2="INSERT INTO `awards`(`user_id`, `title`, `organization`, `description`, `location`, `year`)  VALUES (:user_id, :title, :organization, :description, :location, :yea) ";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(array(
                ':user_id' => $_SESSION['user_info']['id'],
                ':title' => $this->title,
                ':organization' => $this->organization,
                ':description' => $this->description,
                ':location' => $this->location,
                ':yea' => $this->year,
            ));
            if ($stmt) {
                session_start();
                $_SESSION['msg'] = "Successfully Inserted !";
                header('location:view.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index()
    {
        try {

            $query_for_show = "SELECT * FROM `awards` WHERE user_id=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->prepare($query_for_show);
            $stmt->execute();
            $test = $stmt->fetch();

            return $test;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){


            try {
                $query2="UPDATE `awards` SET `user_id`=:user_id,`title`=:title,`organization`=:organization,`description`=:description,`location`=:location,`year`=:yea WHERE user_id=".$_SESSION['user_info']['id'];
                $stmt = $this->pdo->prepare($query2);
                $stmt->execute(array(
                    ':user_id' => $_SESSION['user_info']['id'],
                    ':title' => $this->title,
                    ':organization' => $this->organization,
                    ':description' => $this->description,
                    ':location' => $this->location,
                    ':yea' => $this->year,
                ));
            if($stmt) {
                session_start();
                $_SESSION['message'] = "sucsessfully updated";
                header("location:view.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function deleted(){
        session_start();
        try {
            $query ="DELETE FROM `awards` WHERE `user_id`=".$_SESSION['user_info']['id'];
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "sucsessfully deleted";
                header("location:add.php");
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }



}