<?php
include_once("../../vendor/autoload.php");
use App\About\About;
$object_about = new About();
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $object_about->setData($_POST)->update();
}
else{
    header('location:view.php');
}