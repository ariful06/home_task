<?php

include_once('../vendor/autoload.php');
use App\User_class\User_class;
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $obj=new User_class();
    if(!empty($_POST['username']&& $_POST['email'] && $_POST['repeat_email'] && $_POST['password'])) {
        if ($_POST['email'] == $_POST['repeat_email']) {
            $available = $obj->setData($_POST)->validity();
            if (empty($available)) {
                $obj->setData($_POST)->store();
            }
            else {
                session_start();
                $_SESSION['fail'] = "Username or Email already exists";
                header('location:registrationForm.php');
            }
        } else {
            session_start();
            $_SESSION['fail']= "Email dose not match";
            header('location:registrationForm.php');
        }
    }
    else{
        if(empty($_POST['user_name'])){
            session_start();
            $_SESSION['fail'] = "User Name Required";
            header('location:registrationForm.php');
        }
        if(empty($_POST['password'])){
            session_start();
            $_SESSION['fail'] = "Password must be Required";
            header('location:registrationForm.php');
        }
    }


}
else{

    header('location:registrationForm.php');
}