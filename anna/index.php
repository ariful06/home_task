<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>incognito</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/dashboard.js"></script>
	<!-- /theme JS files -->

</head>
<body>
	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img src="assets/images/logo.png" alt="our logo"></a>
		</div>
		<div class="navbar-collapse collapse" id="navbar-mobile">

			<p class="navbar-text"><span class="label bg-success-400"><?php if($_SESSION['user_info']['is_active']==1){
			    echo "Online";
			}
			else {
			    echo "Offline";
            }
                    ?></span></p>

			<ul class="nav navbar-nav navbar-right">

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-bubbles4"></i>
						<span class="visible-xs-inline-block position-right">Messages</span>
						<span class="badge bg-warning-400">0</span>
					</a>
					
					<div class="dropdown-menu dropdown-content width-350">


						<ul class="media-list dropdown-content-body">
								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold"></span>
									</a>
									<span class="text-muted">Welcome</span>
								</div>
							<li class="media">
								<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold"><?php echo $_SESSION['user_info']['username']?></span>
<!--										<span class="media-annotation pull-right">Mon</span>-->
									</a>
									<span class="text-muted">Welcome to our site</span>
								</div>
							</li>
						</ul>

						<div class="dropdown-content-footer">
							<a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/placeholder.jpg" alt="">
						<span>Annna</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="#"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>Settings</span></a>
									<ul>
										<li><a href="#">View</a></li>
										<li><a href="#">Update</a></li>

									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-copy"></i> <span>About</span></a>
									<ul>
										<li><a href="" id="layout2">View</a></li>
										<li><a href="" id="layout3">Update</a></li>
                                        <li><a href="#">Hobbies</a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Facts</a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </li>

									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-droplet2"></i> <span>Resume</span></a>
									<ul>
										<li><a href="#">Education</a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </li>
										<li><a href="#">Proffesion Skills</a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </li>
										<li><a href="#">Awards</a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </li>

									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-stack"></i> <span>Publications</span></a>
									<ul>
										<li><a href="#">Research</a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </li>
										<li><a href="#">Project</a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </li>

									</ul>
								</li>
								<li><a href="#"><i class="icon-list-unordered"></i> <span>Teaching</span></a>
                                    <ul>
                                        <li><a href="#"><i class="icon-list-unordered"></i> <span>Current Teachig</span></a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#"><i class="icon-list-unordered"></i> <span>Teaching History</span></a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>

                                </li>
								<!-- /main -->

								<!-- Forms -->
								<li>
									<a href="#"><i class="icon-pencil3"></i> <span>Skills</span></a>
                                    <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                    </ul>
								</li>
								<li>
									<a href="#"><i class="icon-footprint"></i> <span>Works</span></a>
                                            <ul>
                                                <li><a href="#">View</a></li>
                                                <li><a href="#">Update</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
								</li>
								<li>
									<a href="#"><i class="icon-spell-check"></i> <span>Contact</span></a>
									<ul>
                                        <li><a href="#">View</a></li>
                                        <li><a href="#">Delete</a></li>
									</ul>
								</li>
								<!-- /main nav bar -->
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->

					<!-- Dashboard content -->

					<!-- /dashboard content -->


					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2017. <a href="#">I N C O G N I T O</a> by <a href=#" target="_blank">INCOGNITO</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
